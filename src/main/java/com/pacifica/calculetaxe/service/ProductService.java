package com.pacifica.calculetaxe.service;

import com.pacifica.calculetaxe.models.Product;

public interface ProductService {
    Product createProductFromOrderLine(String orderLine, int quantity);
}
