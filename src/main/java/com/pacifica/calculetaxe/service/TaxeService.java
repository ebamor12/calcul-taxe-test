package com.pacifica.calculetaxe.service;

import com.pacifica.calculetaxe.models.OrderDetail;
import com.pacifica.calculetaxe.models.Product;

public interface TaxeService {
    double countTotalProductPriceWithoutTaxe(OrderDetail orderDetail );
    double countTaxeOfProduct(Product product, double totalPriceWithouTaxe);
    double countProductTotalPriceTTC(OrderDetail orderDetail);
}
