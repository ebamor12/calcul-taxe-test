package com.pacifica.calculetaxe.common.utils;

import com.pacifica.calculetaxe.common.constant.Constant;
import com.pacifica.calculetaxe.models.Invoice;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileUtil {
    public static File createNewFile(Invoice invoice){
        Date date = new Date() ;
        SimpleDateFormat dateFormat = new SimpleDateFormat(Constant.DATE_PATTERN) ;
        return new File(Constant.TEST_PATH+Constant.FILE_NAME+ invoice.getTotalePriceAmount()+"_"+dateFormat.format(date)+Constant.TXT_EXTENSION) ;
    }

    public static void createTextInFile(File file, String text){
        try (FileWriter fw = new FileWriter(file, true);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter out = new PrintWriter(bw)) {
             out.println(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private FileUtil() {
    }
}
