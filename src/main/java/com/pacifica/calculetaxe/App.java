package com.pacifica.calculetaxe;
import java.io.IOException;

import com.pacifica.calculetaxe.common.constant.Constant;

import com.pacifica.calculetaxe.models.Invoice;
import com.pacifica.calculetaxe.service.InvoiceService;
import com.pacifica.calculetaxe.service.OrderService;
import com.pacifica.calculetaxe.service.impl.InvoiceServiceImpl;
import com.pacifica.calculetaxe.service.impl.OrderServiceImpl;

public class App {
    public static void main(String[] args) throws IOException {

        OrderService orderServiceImpl = new OrderServiceImpl();
        InvoiceService invoiceServiceImpl = new InvoiceServiceImpl();

        Invoice invoice1 = invoiceServiceImpl
                .createInvoiceFromOrder(orderServiceImpl.createOrderFromFile(Constant.FIRST_COMMAND_FILE_PATH));
        Invoice invoice2 = invoiceServiceImpl
                .createInvoiceFromOrder(orderServiceImpl.createOrderFromFile(Constant.SECOND_COMMAND_FILE_PATH));
        Invoice invoice3 = invoiceServiceImpl
                .createInvoiceFromOrder(orderServiceImpl.createOrderFromFile(Constant.THIRD_COMMAND_FILE_PATH));

        invoiceServiceImpl.createInvoiceFile(invoice1);
        invoiceServiceImpl.createInvoiceFile(invoice2);
        invoiceServiceImpl.createInvoiceFile(invoice3);

    }

}
